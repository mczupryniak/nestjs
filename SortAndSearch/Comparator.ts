export type Comparator = (a: number, b: number) => boolean;

export const defaultComparator: Comparator = (a: number, b: number): boolean => {
  return a < b;
};
