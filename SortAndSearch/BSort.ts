import { defaultComparator, Comparator } from './Comparator';

// quicksort
export class BSort {
  private _comparator: Comparator;
  private _sortedArray: number[];

  constructor(arrayToSort: number[], comparator?: Comparator) {
    this._sortedArray = [ ...arrayToSort ];
    this._comparator = comparator ? comparator : defaultComparator;
    this.recursiveSort(0, this._sortedArray.length - 1);
  }

  get sortedArray(): number [] {
    return this._sortedArray;
  }

  private recursiveSort(start: number, end: number) {
    if (end - start < 1) {
      return;
    }

    const pivotValue = this._sortedArray[end];
    let splitIndex: number = start;

    for (let i: number = start; i < end; i++) {
      const sort: boolean = this._comparator(this._sortedArray[i], pivotValue);

      if (sort) {
        if (splitIndex !== i) {
          const temp = this._sortedArray[splitIndex];
          this._sortedArray[splitIndex] = this._sortedArray[i];
          this._sortedArray[i] = temp;
        }

        splitIndex++;
      }
    }

    this._sortedArray[end] = this._sortedArray[splitIndex];
    this._sortedArray[splitIndex] = pivotValue;

    this.recursiveSort(start, splitIndex - 1);
    this.recursiveSort(splitIndex + 1, end);
  };
}
