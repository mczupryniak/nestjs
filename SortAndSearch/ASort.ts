import { defaultComparator, Comparator } from './Comparator';

// mergesort
export class ASort {
  private _comparator: Comparator;
  private _sortedArray: number[];

  constructor(arrayToSort: number[], comparator?: Comparator) {
    this._comparator = comparator ? comparator : defaultComparator;
    this._sortedArray = this.mergeSort([ ...arrayToSort ]);
  }

  get sortedArray(): number [] {
    return this._sortedArray;
  }

  private mergeSort(array: number[], half = array.length/2): number[] {
    if(array.length < 2){
      return array;
    }
  
    const left = array.splice(0, half);
  
    return this.merger(this.mergeSort(left), this.mergeSort(array));
  }

  private merger(left: number[], right: number[]): number[] {
    const arr: number[] = [];

    while (left.length && right.length) {
      this._comparator(left[0], right[0]) ? arr.push(left.shift()) : arr.push(right.shift());
    }

    return [ ...arr, ...left, ...right ];
  }
}