export class BSearch {
  private static instance: BSearch;
  private _operations: number = 0;

  static getInstance(): BSearch {
    if (!BSearch.instance) {
      BSearch.instance = new BSearch();
    }

    return BSearch.instance;
  }

  get operations(): number {
    return this._operations;
  }

  getElementIndex(array: number[], element: number): number {
    this._operations++;
    
    let left = 0;
    let right = array.length;

    while (left <= right) {
      const pivot = Math.floor((left + right) / 2);

      if (array[pivot] < element) {
        left = pivot + 1;
      } else if (array[pivot] > element) {
        right = pivot - 1;
      } else {
        return pivot;
      }
    }

    return -1;
  };
}
