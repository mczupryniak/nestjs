import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

// spec:

// sorting
const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];

const defaultComparatorSorted = [0, 2, 4, 5, 7, 9, 13, 14, 17, 22, 32, 65, 77, 83];
const customComparatorSorted = [83, 77, 65, 32, 22, 17, 14, 13, 9, 7, 5, 4, 2, 0];

const customComparator = (a: number, b: number) => a > b;

const aSort = new ASort(unsorted);
const aSorted = JSON.stringify(aSort.sortedArray) === JSON.stringify(defaultComparatorSorted);

const aSortCustom = new ASort(unsorted, customComparator);
const aCustomSorted = JSON.stringify(aSortCustom.sortedArray) === JSON.stringify(customComparatorSorted);

const bSort = new BSort(unsorted);
const bSorted = JSON.stringify(bSort.sortedArray) === JSON.stringify(defaultComparatorSorted);

const bSortCustom = new BSort(unsorted, customComparator);
const bCustomSorted = JSON.stringify(bSortCustom.sortedArray) === JSON.stringify(customComparatorSorted);


// bSearch
const elementsToFind = [1, 5, 13, 27, 77];

const bSearchFirst = BSearch.getInstance();
const founded27 = bSearchFirst.getElementIndex(elementsToFind, 27) === 3;
const founded5 = bSearchFirst.getElementIndex(elementsToFind, 5) === 1;

const bSearchSecond = BSearch.getInstance();
const founded77 = bSearchSecond.getElementIndex(elementsToFind, 77) === 4;
const founded9 = bSearchSecond.getElementIndex(elementsToFind, 7) === -1;

const numberOfOperations = bSearchFirst.operations === 4;

console.log('Tests:');
console.log({
  aSorted,
  aCustomSorted,
  bSorted,
  bCustomSorted,
  founded27,
  founded5,
  founded77,
  founded9,
  numberOfOperations
});
